# Step-Project: Cards
The project was carried out by:<br/>
**Kateryna Biehantseva, Maksym Korchak, Vrindavan Islamov.**
##Task Allocation:
####Maksym Korchak:
​
- Title page
- Module:
  - App.js
  - Modal.js
- Drag and Drop
- Appropriate CSS for modules above
​
####Vrindavan Islamov:
​
- Module:
  - Form.js
  - Visit.js
- Appropriate CSS for modules above
​
####Kateryna Biehantseva:
​
- Module:
  - Form.js
  - Visit.js
- Appropriate CSS for modules above
​
###Team Work:
​
- Building project
- Task allocation
- Code review
- Fetch queries
​
During the project development <br>
we used following technologies and approaches as:
- 