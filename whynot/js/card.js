import {VisitCardiologist, VisitDentist, VisitTherapist} from './Visit.js'
import {TaskDesk} from "./desk.js";
import {Form} from "./Form.js";

export class Card {
    constructor(kard) {
        this.id = kard.id;
        this.kard = kard;

        this.elements = {
            cardContainer: document.createElement("div"),
            editBtn: document.createElement('button'),
            detailsBtn: document.createElement('button'),
            deleteBtn: document.createElement('button'),
            doctorDetailsFields: document.createElement('div'),
            editCardContainer: document.createElement("div")
        }
    }
    render() {
        this.elements.cardContainer.classList.add('card');
        this.elements.editBtn.classList.add('edit-btn');
        this.elements.detailsBtn.classList.add('details-btn');
        this.elements.deleteBtn.classList.add('delete-btn');
        this.elements.doctorDetailsFields.classList.add('doctorDetails');
        this.elements.editCardContainer.classList.add('editCardContainer');
        this.elements.editBtn.innerText = 'Edit';
        this.elements.detailsBtn.innerText = 'Details';
        this.elements.deleteBtn.innerText = 'X';


        /*   loop to fill cards fields */

        const cardContentInfo = Object.entries(this.kard);
        cardContentInfo.forEach(([key, value]) => {

                if (key.toLowerCase() === "age" || key.toLowerCase() === "bodyindex" ||
                    key.toLowerCase() === "illnesses" || key.toLowerCase() === "pressure" ||
                    key.toLowerCase() === "lastvisit" || key.toLowerCase() === "id") {

                    let detailsCardField = document.createElement("p");
                    detailsCardField.classList.add("card-fields");
                    detailsCardField.innerText = `${key.toUpperCase()}: ${value}`;
                    this.elements.doctorDetailsFields.append(detailsCardField);

                } else {
                    let mainCardField = document.createElement("p");
                    mainCardField.classList.add("card-fields");
                    mainCardField.innerText = `${key.toUpperCase()}: ${value}`;
                    this.elements.cardContainer.append(mainCardField);
                }
                // this.elements.cardContainer.append(this.elements.doctorDetailsFields);
            // }
        })

        this.elements.cardContainer.append(this.elements.editBtn, this.elements.deleteBtn);
        this.elements.cardContainer.insertAdjacentElement('beforeend', this.elements.detailsBtn);
        document.querySelector('.task-container').append(this.elements.cardContainer);


        /*   Card Btn Listeners   */

        this.elements.deleteBtn.addEventListener('click', () => this.deleteCardReqvest());
        this.elements.detailsBtn.addEventListener('click', () => {
            if (!document.querySelector('.doctorDetails')) {
                this.elements.cardContainer.append(this.elements.doctorDetailsFields);
                this.elements.detailsBtn.innerText = 'Hide';
            } else {
                this.elements.detailsBtn.innerText = 'Details';
                document.querySelector('.doctorDetails').remove();
            }
        })
        this.elements.editBtn.addEventListener('click', () => this.editCard());
        this.dragDrop();
    }

    dragDrop() {
        const allCards = document.querySelectorAll(".card");
        allCards.forEach(card => {
            card.onmousedown = function (event) {
                if ((event.target.classList.contains('details-btn') || event.target.classList.contains('edit-btn') || event.target.classList.contains('delete-btn'))) {
                    return;
                }
                let shiftX = event.clientX - card.getBoundingClientRect().left;
                let shiftY = event.clientY - card.getBoundingClientRect().top;
                card.style.position = "absolute";
                card.style.zIndex = 1000;
                card.style.cursor = "grab";
                document.body.append(card);
                movePosElem(event.pageX, event.pageY);

                function movePosElem(pageX, pageY) {
                    card.style.left = pageX - shiftX + 'px';
                    card.style.top = pageY - shiftY + 'px';
                }

                function onMouseMove(event) {
                    movePosElem(event.pageX, event.pageY);
                }

                card.ondragstart = function () {
                    return false;
                };
                document.addEventListener('mousemove', onMouseMove);
                card.onmouseup = function () {
                    document.removeEventListener('mousemove', onMouseMove);
                    card.onmouseup = null;
                };
            };
        })
    }
    deleteCardReqvest() {
        fetch(`https://cards.danit.com.ua/cards/${this.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`
            },
        }).then((res) => {
            if (res.status >= 200 && res.status < 300) {
                return res;
            } else {
                let error = new Error(res.statusText);
                error.response = res;
                throw error
            }
        })
            .then((res) => res.json())
            .then(res => {
                if (res.status === 'Success') {
                    this.elements.cardContainer.remove();
                    console.log('аяяяя')
                    this.elements.deleteBtn.removeEventListener('click', () => this.deleteCardReqvest());
                }
            })
    }


    editCard() {
        const removeBtn = document.querySelectorAll(".edit-btn");
        removeBtn.forEach(e => e.classList.add("remove"));

        if (this.kard.title.toLowerCase() === "cardiologist") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormCardiologist = new VisitCardiologist(this.kard);
            editFormCardiologist.render(this.elements.editCardContainer);

            let formBttn = document.querySelector('.form-submit');
            console.log(formBttn);
            this.saveEditRequest(kard)


        }
        if (this.kard.title.toLowerCase() === "therapist") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormVisitTherapist = new VisitTherapist(this.kard);
            editFormVisitTherapist.render(this.elements.editCardContainer);
        } else if (this.kard.title.toLowerCase() === "dentist") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormVisitDentist = new VisitDentist(this.kard);
            editFormVisitDentist.render(this.elements.editCardContainer);

            document.querySelector('.visit-form').addEventListener('submit', async (e) => {
                e.preventDefault();
                e.stopPropagation();

              alert('jjjj');
                this.saveEditRequest(this.kard)
            })


            // document.querySelector("visit-form").removeEventListener('submit', async (e) => {
            //     await this.saveEditRequest(this.kard)
            // })
        }
    }

    saveEditRequest({description}) {
        alert('aaaaaaaa');
let y = document.querySelector('form')
        console.log(y);
        let r = new FormData(document.querySelector('form'));
        console.log(r);
    }
        // this.elements.cardContainer.append(this.elements.doctorDetailsFields);
            // }
        // })
        // const c = Object.entries(this.kard);
        // const r = c.map(([key, value]) => {
        //      {`${key} : ${value}`}
        //     console.log(key, value);
        // })
        //

        //     fetch(`https://cards.danit.com.ua/cards/${kard.id}`, {
        //     method: 'PUT',
        //     body: JSON.stringify({
        //         purpose: `${purpose.value}`,
        //         description: `${description.value}`,
        //         urgency: `${urgency.value}`,
        //         name: `${name.value}`,
        //         index: `${this.index.value}`,
        //         pressure: `${this.pressure.value}`,
        //         illnesses: `${this.illnesses.value}`,
        //         age: `${this.age.value}`
        //     }),
        //     headers: {
        //         'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        //     }
        // })
        //     .then((res) => {
        //         if (res.status >= 200 && res.status < 300) {
        //             return res;
        //         } else {
        //             let error = new Error(res.statusText);
        //             error.response = res;
        //             throw error
        //         }
        //     })
        //     .then(res => res.json())
        //     .then(data => {
        //         if (data.status === 'Error') {
        //             alert(data.message)
        //         }
        //         // else {
        //         //     // window.location.reload()
        //         // }
        //     })
        // const m = new TaskDesk().clearCards().getCards();
        //      // m.getCards();
    }





    // }

